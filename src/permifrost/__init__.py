# Managed by bumpversion
__version__ = "0.15.1"

from permifrost.error import SpecLoadingError

__all__ = [
    "SpecLoadingError",
]
